/* eslint-disable no-unused-vars */
import {
  singleData, multipleData, judgeData, FormData, preservation
} from './formatData'

// 试卷原型
class Paper {
  constructor () {
    this.FormData = FormData
  }
  log () {
    console.log('i am a paper')
  }
}
export {
  Paper, FormData
}
