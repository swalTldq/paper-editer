//  paper 的标准模板数据
const singleData = {
  id: 'S-202106060001',
  // 题目
  subject: '请填写单选题题目',
  // 单选选项
  options: [
    '选项A', '选项B', '选项C', '选项D'
  ],
  // 正确答案 学生考试生成的试卷答案为选项
  answer: 'A',
  // 考生填写的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  // 是否正在编辑
  isEdit: false
}
const multipleData = {
  id: 'M-202106060001',
  // 题目
  subject: '请填写多选题题目',
  // 多选选项
  options: [
    '选项A', '选项B', '选项C', '选项D'
  ],
  // 正确答案
  answer: ['A', 'B'],
  // 考生的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  // 是否正在编辑
  isEdit: false
}
const judgeData = {
  id: 'J-202106060001',
  // 题目
  subject: '请填写判断题题目',
  // 多选选项
  options: null,
  // 答案
  answer: 0,
  // 考生的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  // 是否正在编辑
  isEdit: false
}
const programmingData = {
  id: 'J-202106060001',
  // 题目
  subject: '请填写编程题题目',
  // 多选选项
  options: null,
  // 答案
  answer: 0,
  // 考生的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  // 是否正在编辑
  isEdit: false
}
const FormData = {
  title: '北软信息职业技术学院试卷模板 ',
  totalScore: '',
  createPeople: '李大庆',
  singleData: [singleData],
  multipleData: [multipleData],
  judgeData: [judgeData],
  programmingData: []
}
const preservation = {
  singleType: {
    subject: String,
    options: [
      {'A': 'a'}, {'B': 'b'}, {'C': 'c'}, {'D': 'd'}
    ],
    answer: Array,
    studentAnswer: Array,
    analysis: String,
    score: Number
  },
  multipleType: {
    // 题目
    subject: '',
    // 多选选项
    options: [
      'A', 'B', 'C', 'D', 'D'
    ],
    // 正确答案
    answer: Array,
    // 考生的答案
    studentAnswer: Array,
    // 解析
    analysis: String,
    // 分值
    score: Number
  },
  judgeType: {
    // 题目
    subject: String,
    // 答案 true / false
    answer: Boolean,
    // 考生的答案
    studentAnswer: Boolean,
    // 解析
    analysis: String,
    // 分值
    score: Number
  },
  subjectType: {
    // 题目
    subject: String,
    // 答案
    answer: String,
    // 考生的答案
    studentAnswer: String,
    // 解析
    analysis: String,
    // 分值
    score: Number
  }
}
export {
  singleData, multipleData, judgeData, programmingData, FormData, preservation
}
